﻿using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace WpfApp1
{
    /// <summary>
    /// Logika interakcji dla klasy MainWindow.xaml
    /// </summary>
       public partial class MainWindow : Window
    {
        BitmapImage Photo;

         ObservableCollection<Person> people = new ObservableCollection<Person>
        {
           new Person { Name="P1", Age=1},
           new Person {Name="P2", Age=2}
        };
        public ObservableCollection<Person> Items
        {
            get => people;
        }

        public MainWindow()
        {
            InitializeComponent();
            DataContext = this;
        }


        private void AddNewPersonButton_Click(object sender, RoutedEventArgs e)
        {
            string name = nameTextBox.Text.ToString();

            try
            {
                if (name == "") throw new System.FormatException();
                people.Add(new Person { Age = int.Parse(ageTextBox.Text), Name = name, ProfilePhoto = Photo});
            }
            catch (System.FormatException)
            {
                MessageBox.Show("Wpisz prawidłowe dane");
            }
        }
    private void Button_Click(object sender, RoutedEventArgs e)
        {
            OpenFileDialog op = new OpenFileDialog();
            op.Title = "Select a picture";
            op.Filter = "All supported graphics|*.jpg;*.jpeg;*.png|" +
              "JPEG (*.jpg;*.jpeg)|*.jpg;*.jpeg|" +
              "Portable Network Graphic (*.png)|*.png";
            if (op.ShowDialog() == true)
            {
                Obraz.Source = new BitmapImage(new Uri(op.FileName));
                Photo = new BitmapImage(new Uri(op.FileName));
            }
        }

        private void ListBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {

        }
    }
}


